<?php

namespace controladores;

class siteController extends Controller {

    private $miPie;
    private $miMenu;

    public function __construct() {
        parent::__construct();
        $this->miPie = "Ejercicios Practica 6";
        $this->miMenu = [
            "Inicio" => $this->crearRuta(["accion" => "index"]),
            "Hospitales y Residencias" => $this->crearRuta(["accion" => "hospitalesResidencias"]),
            "Restaurantes y Bares" => $this->crearRuta(["accion" => "restaurantesBares"]),
            "Parada de Autobuses" => $this->crearRuta(["accion" => "paradaAutobuses"]),
        ];
    }

    public function indexAccion() {
        $this->render([
            "vista" => "index",
            "pie" => $this->miPie,
            "menu" => (new \clases\Menu($this->miMenu, "Inicio"))->html(),
            "menu1" => (new \clases\Menu([
                       "Hospitales y Residencias" => $this->crearRuta(["accion" => "hospitalesResidencias"]),
                       "Restaurantes y Bares" => $this->crearRuta(["accion" => "restaurantesBares"]),
                       "Parada de Autobuses" => $this->crearRuta(["accion" => "paradaAutobuses"]),
                       ], "Inicio"))
                    ->html()
        ]);
    }
    
    public function hospitalesResidenciasAccion($datos) {
        $vista = "hospitalesResidencias";
        $pie = "hospitalesResidencias";
        $salida="";
        if (!empty($datos->getValores())) {
            $pie = $this->miPie;
            if(isset($_SESSION["mapa"])){
                $mapa= unserialize($_SESSION["mapa"]);
            }else{
                $mapa=new \clases\Mapa();
            }
            $mapa->setHospitales(new \clases\Hospital($datos->getValores()['coordenada']));
            $vista = "resultado";
            $pie = $this->miPie;
            $salida= $mapa->comenzarMapa();
            $salida.= $mapa->dibujarHospital();
            $salida.= $mapa->terminarMapa();
            //Esta en el destructor
            //$_SESSION["mapa"]= serialize($mapa);
        } 

        $this->render([
            "vista" => $vista,
            "pie" => $pie,
            "menu" => (new \clases\Menu($this->miMenu, "Hospitales y Residencias"))->html(),
            "resultado"=>$salida,
        ]);
    }
    
    public function restaurantesBaresAccion($datos) {
        $vista = "restaurantesBares";
        $pie = "restaurantesBares";  
        $salida="";
        if (!empty($datos->getValores())) {
            $pie = $this->miPie;
            if(isset($_SESSION["mapa"])){
                $mapa= unserialize($_SESSION["mapa"]);
            }else{
                $mapa=new \clases\Mapa();
            }
            
            $mapa->setRestaurante(new \clases\Restaurante($datos->getValores()['coordenada']));
            $vista = "resultado";
            $pie = $this->miPie;
            $salida= $mapa->comenzarMapa();
            $salida.= $mapa->dibujarRestaurante();
            $salida.= $mapa->terminarMapa();
            //Esta en el destructor
            //$_SESSION["mapa"]= serialize($mapa);
        }  
            $this->render([
            "vista" => $vista,
            "pie" => $pie,
            "menu" => (new \clases\Menu($this->miMenu, "Restaurantes y Bares"))->html(),
            "resultado"=>$salida,    
        ]);
    }
   
    public function paradaAutobusesAccion($datos) {
        $vista = "paradaAutobuses";
        $pie = "paradaAutobuses";
        $salida="";
        if (!empty($datos->getValores())) {
            $pie = $this->miPie;
            if(isset($_SESSION["mapa"])){
                $mapa= unserialize($_SESSION["mapa"]);
            }else{
                $mapa=new \clases\Mapa();
            }
            
            $mapa->setParadaAutobuses(new \clases\Autobuses($datos->getValores()['coordenada']));
            $vista = "resultado";
            $pie = $this->miPie;
            $salida= $mapa->comenzarMapa();
            $salida.= $mapa->dibujarAutobuses();
            $salida.= $mapa->terminarMapa();
            //Esta en el destructor
            //$_SESSION["mapa"]= serialize($mapa);
        }  
        
            $this->render([
            "vista" => $vista,
            "pie" => $pie,
            "menu" => (new \clases\Menu($this->miMenu, "Parada de Autobuses"))->html(),
            "resultado"=>$salida,    
        ]);
    }
    
    public function filtrarAccion($datos){
        //var_dump($datos);
        $mapa= unserialize($_SESSION["mapa"]);
        $vista = "resultado";
        $pie = "resultado";
        $salida="";
        //var_dump($datos->getValores()["seleccion"]);
        $salida = $mapa->comenzarMapa();
        foreach ($datos->getValores()["seleccion"] as $valor){
            if ($valor=='hospitales'){
                $salida.= $mapa->dibujarHospital($datos->getValores()["seleccion"]);
            }else if ($valor=='restaurantes'){
                $salida.= $mapa->dibujarRestaurante($datos->getValores()["seleccion"]);
            } else if ($valor =='autobuses'){
                $salida.= $mapa->dibujarAutobuses($datos->getValores()["seleccion"]);   
            }
        }
        $salida.=$mapa->terminarMapa();
        
        $this->render([
            "vista" => "resultado",
            "pie" => "dibujando mapa",
            "menu" => (new \clases\Menu($this->miMenu, "Hospitales y Residencias"))->html(),
            "resultado"=>$salida,
        ]);
        
    }
    
    
    
}
