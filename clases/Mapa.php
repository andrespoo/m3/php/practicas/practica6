<?php

namespace clases;

class Mapa {
    public $hospitales=[];
    public $restaurante=[];
    public $paradaAutobuses=[];

    public function getParadaAutobuses() {
        return $this->paradaAutobuses;
    }

    public function setParadaAutobuses(Autobuses $paradaAutobuses) {
        $this->paradaAutobuses[] = $paradaAutobuses;
        return $this;
    }

        public function getRestaurante() {
        return $this->restaurante;
    }

    public function setRestaurante(Restaurante $restaurante) {
        $this->restaurante[] = $restaurante;
        return $this;
    }

        public function getHospitales() {
        return $this->hospitales;
    }

    public function setHospitales(Hospital $hospital) {
        $this->hospitales[]=$hospital;
        return $this;
    }
    
    public function __construct() {
    }
    
    public function comenzarMapa(){
        $salida='<svg width="800" height= "800">';
        return $salida;
    }
    
    public function terminarMapa(){
        $salida='</svg>';
        return $salida;
    }
    
    public function dibujarHospital(){
        $salida="";
        foreach ($this->hospitales as $h) {
            $salida.=$h->dibujar();
        }
        
        return $salida;
    }
    
    public function dibujarRestaurante(){
        $salida="";
        foreach ($this->restaurante as $h) {
            $salida.=$h->dibujar();
        }

        return $salida;
    }
    
    public function dibujarAutobuses(){
        $salida="";
        foreach ($this->paradaAutobuses as $h) {
            $salida.=$h->dibujar();
        }

        return $salida;
    }
    
    
    public function __destruct() {
        $_SESSION["mapa"]= serialize($this);
    }


    
}
