<?php

namespace clases;


class Elemento {
    public $coordX=0;
    public $coordY=0;
    public $simbolo;
    
    function __construct(array $coordenadas) {
        $this->simbolo="-";
        $this->coordX = $coordenadas[0];
        $this->coordY = $coordenadas[1];
        
    }
    
    public function getCoordX() {
        return $this->coordX;
    }

    public function getCoordY() {
        return $this->coordY;
    }

    public function setCoordX($coordX) {
        $this->coordX = $coordX;
        return $this;
    }

    public function setCoordY($coordY) {
        $this->coordY = $coordY;
        return $this;
    }

    public function dibujar(){
        $salida='<text ';
        $vector = [
          "x" => $this->getCoordX(),
          "y" => $this->getCoordY(),
         ];  
                   
        foreach ($vector as $k => $v) {
              $salida.= $k . '=' . ' "' .$v . '"';
        }
        $salida.='>';
        $salida.= $this->simbolo;
        $salida.='</text>';
        return $salida;
    }
}
